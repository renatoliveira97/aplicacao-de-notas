import express from 'express';
import { v4 as uuidv4 } from 'uuid';

const app = express();

let users = [];

const User = (name, cpf) => {
    let object = {};

    object.id = uuidv4();
    object.name = name;
    object.cpf = cpf;
    object.notes = [];

    return object;
};

const Note = (title, content) => {
    let object = {};

    object.id = uuidv4();
    object.title = title;
    object.content = content;
    object.created_at = new Date();

    return object;
}

const checkCPF = (req, res, next) => {
    try {
        const { cpf } = req.params;

        const userIndex = users.findIndex(user => user.cpf === cpf);

        if (userIndex === -1) {
            return res.status(404).json({
                error: "invalid cpf - user is not registered"
            });
        } 

        req.user = users[userIndex];
        req.userIndex = userIndex;
        next();
    } catch (e) {
        return res.status(400).json({
            error: e.message
        })
    }
}

app.use(express.json());

app.get('/users', (req, res) => {
    return res.status(200).json(users);
});

app.post('/users', (req, res) => {
    try {
        const { name, cpf } = req.body;
        
        const userIndex = users.findIndex(user => user.cpf === cpf);

        if (userIndex === -1 && name !== undefined && cpf !== undefined) {
            let user = User(name, cpf);
            users.push(user);
    
            return res.status(201).json(user);
        } else {
            return res.status(400).json({
                error: "Error creating user"
            });
        }        
    } catch (e) {
        return res.status(400).json({
            error: e.message
        })
    }        
});

app.put('/users/:cpf', checkCPF, (req, res) => {
    try{
        const { name } = req.body;

        users[req.userIndex].name = name;

        return res.status(200).json({
            message: "User is updated",
            users: users
        });
    } catch (e) {
        return res.status(400).json({
            error: e.message
        });
    }
});

app.delete('/users/:cpf', checkCPF, (req, res) => {
    users.splice(req.userIndex, 1);

    return res.status(200).json({
        message: "User is deleted",
        users: users
    });
});

app.get('/users/:cpf/notes', checkCPF, (req, res) => {
    let data = users[req.userIndex].notes;

    return res.status(200).json(data);
});

app.post('/users/:cpf/notes', checkCPF, (req, res) => {
    try{
        const { title, content } = req.body;

        if (title !== undefined && content !== undefined) {
            let note = Note(title, content);

            users[req.userIndex].notes.push(note);

            return res.status(201).json({
                message: `${title} was added into ${users[req.userIndex].name}'s notes`
            });
        } else {
            return res.status(400).json({
                error: "Error creating note"
            });
        }
    } catch (e) {
        return res.status(400).json({
            error: e.message
        });
    }
});

app.put('/users/:cpf/notes/:id', checkCPF, (req, res) => {
    try {
        const { title, content } = req.body;
        const { id } = req.params;

        const noteIndex = users[req.userIndex].notes.findIndex(note => note.id === id);

        if (noteIndex > -1) {
            if (title !== undefined) {
                users[req.userIndex].notes[noteIndex].title = title;
            }

            if (content !== undefined) {
                users[req.userIndex].notes[noteIndex].content = content;
            }

            users[req.userIndex].notes[noteIndex].updated_at = new Date();

            return res.status(200).json(users[req.userIndex].notes[noteIndex]);
        } else {
            return res.status(404).json({
                error: "Note not find"
            })
        }
    } catch (e) {
        return res.status(400).json({
            error: e.message
        });
    }
});

app.delete('/users/:cpf/notes/:id', checkCPF, (req, res) => {
    try{
        const { id } = req.params;

        const noteIndex = users[req.userIndex].notes.findIndex(note => note.id === id);

        if (noteIndex === -1) {
            return res.status(404).json({
                error: "invalid id - note doesn't exists"
            });
        }

        users[req.userIndex].notes.splice(noteIndex, 1);

        return res.status(200).json(users[req.userIndex].notes);
    } catch (e) {
        return res.status(400).json({
            error: e.message
        });
    }    
});

app.listen(3000, () => {
    console.log('Running at http://localhost:3000');
});

export default app;
